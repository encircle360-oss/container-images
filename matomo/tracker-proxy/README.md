# Matomo Tracker Proxy Docker Image
Short example usage description to launch the proxy on host port 8085:
```
docker run -p 8085:80 --volume /path/to/tracker-proxy-config.php:/www/html/config.php registry.gitlab.com/encircle360-oss/container-images:matomo-tracker-proxy-1.0
```

If running in kubernetes / k8s context just mount the config via ConfigMap to `/www/html/config.php`.

An example config can be found [here](https://github.com/matomo-org/tracker-proxy/blob/master/config.php.example).
